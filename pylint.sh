#!/usr/bin/env bash

docker run\
  --rm \
  --interactive --tty \
  --name pylint \
  --volume $(pwd):/$(basename $(pwd)) \
  --workdir /$(basename $(pwd)) \
  python:3.9.7 \
  $@

exit 0
