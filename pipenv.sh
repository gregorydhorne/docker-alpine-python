#!/usr/bin/env bash

[ ${1} !~ /[0-9]{4,}/ ] && port=8000
[ ${1} ~ /[0-9]{4,}/ ] && port=${1} && shift

docker run\
  --rm \
  --interactive --tty \
  --name pipenv-${port} \
  --publish ${port}:8000 \
  --volume $(pwd):/$(basename $(pwd)) \
  --workdir /$(basename $(pwd)) \
  pipenv:2021.5.29 \
  $@

exit 0
