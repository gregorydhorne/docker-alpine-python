#!/usr/bin/env bash

docker run\
  --rm \
  --interactive --tty \
  --volume $(pwd):/$(basename $(pwd)) \
  --workdir /$(basename $(pwd)) \
  --env LANG=en_CA.UTF-8 \
  python:3.9.7 ${1}

exit 0
