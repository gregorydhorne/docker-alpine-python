# Docker Image for Python3, pip, pipenv, and pylint

Based on Alpine Linux.


## Retrieve the docker-alpine-python3 Repository

    git clone https://gitlab.com/gregorydhorne/docker-alpine-python.git


## Build the container images

Make the retrieved Git repository the current working directory.

  cd docker-alpine-python

Build the container images for Python and the associated development tools pip and pipenv. Adhere to the order shown below or face the consequences of failed container image builds.

    cp Dockerfile.python Dockerfile
    docker build --tag python:3.9.7 .

    cp Dockerfile.pip Dockerfile
    dcker build --tag pip:20.3.4 .

    cp Dockerfile.pipenv Dockerfile
    docker build --tag pipenv:2021.5.29 .

    cp Dockerfile.pylint Dockerfile
    docker build --tag pylint:2.10.2 .

## Instantiate a container as though it is a natively-installed application

After successfully building the container images (python, pip, pipenv, and pylint), copy the following scripts to a directory in the executable search path, $PATH, adjusting the path below.
Adjust the target path based on your file system structure.

    cp python.sh $HOME/.local/scripts/python
    cp pip.sh $HOME/.local/scripts/pip
    cp pipenv.sh $HOME/.local/scripts/pipenv
    cp pylint.sh $HOME/.local/scripts/pylint

Make these scripts executable.
Adjust the path based on your file system structure.

    chmod +x $HOME/.local/scripts/python
    chmod +x $HOME/.local/scripts/pip
    chmod +x $HOME/.local/scripts/pipenv
    chmod +x $/HOME/.local/scripts/pylint

Update the PATH environmental variable placing $HOME/.local/scripts before any
existing search paths.

    PATH=$HOME/.local/scripts:$PATH

Verify the Python container can be instantiated and can execute a Python programme. Notice python3 can be invoked as though it is natively installed; the same applies to pip and pipenv.

    python hello.py

