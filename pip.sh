#!/usr/bin/env bash

docker run\
  --rm \
  --interactive --tty \
  --volume $(pwd):/$(basename $(pwd)) \
  -- workdir /$(basename $(pwd)) \
  --env LANG=en_CA.UTF-8 \
  pip:20.3.4 \
  $@

exit 0
